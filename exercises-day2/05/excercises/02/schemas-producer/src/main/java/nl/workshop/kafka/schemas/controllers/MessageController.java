package nl.workshop.kafka.schemas.controllers;

import nl.workshop.kafka.avro.schemas.Message;
import nl.workshop.kafka.schemas.events.AvroMessageProducer;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

	private AvroMessageProducer avroMessageProducer;

	public MessageController(AvroMessageProducer avroMessageProducer) {
		this.avroMessageProducer = avroMessageProducer;
	}

	@PostMapping
	public void sendPlayer(@RequestBody final Message message) {
		avroMessageProducer.sendMessage(message);
	}
}
