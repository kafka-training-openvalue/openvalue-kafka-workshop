# Exercise: Check compatibility settings schema registry

1. Start the MessagesProducerApplication and MessagesConsumerApplication and check if you can send and receive messages.  
 E.g:  ```curl --header "Content-Type: application/json"   --request POST   --data '{"id":"1","name":"OrderMessage"}'   http://localhost:20023```  
 Also check  [http://localhost:8081/subjects/test.compatibility-value/versions/1](http://localhost:8081/subjects/test.compatibility-value/versions/1) if schema with name "Message" is registered in the schema-registry
2. Check the compatibility settings  
   [http://localhost:8081/config](http://localhost:8081/config)  
   The default should be "BACKWARD". This is the global compatibility setting.  
   Compatibility can also be set per subject:
   [http://localhost:8081/config/test.compatibility-value](http://localhost:8081/config/test.compatibility-value)  
   When compatibility is not set on subject level it will return a not found exception and then the global compatibility setting is used.
   Set the compatibility of the subject ```test.compatibility-value```  also to "BACKWARD". Which means in this case that all messages sent to the test.compatibility topic have to be backwards compatible.
   ```curl -X PUT -H "Content-Type: application/vnd.schemaregistry.v1+json" --data '{"compatibility": "BACKWARD"}' http://localhost:8081/config/test.compatibility-value```
3. With compatibility set to BACKWARD the  allowed changes are: add optional fields and delete fields. 
   To check this: delete a field or add an optional field (field with default value) in the ```message.avsc``` file of the producer, don't forget to compile, and restart MessagesProducerApplication.
   Then send a message to check if the messages still can be consumed.
4. Now add a required field (field without default value) to the```message.avsc``` file  of the producer, compile and restart again. Try to send a message with this new field. Now you should get this exception "Schema being registered is incompatible with an earlier schema for subject "test.compatibility-value"; error code: 409"   
   Set the compatibility setting of the subject ```test.compatibility-value``` to "FORWARD", and send the message with the new required field again.
   Now the message should be accepted, because with "FORWARD" compatibility allowed changes are: delete optional fields and add fields.


   

   

 






