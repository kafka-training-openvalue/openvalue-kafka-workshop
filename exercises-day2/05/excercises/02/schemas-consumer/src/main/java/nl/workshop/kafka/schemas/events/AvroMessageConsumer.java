package nl.workshop.kafka.schemas.events;

import nl.workshop.kafka.avro.schemas.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;


@Service
public class AvroMessageConsumer {

  private static final Logger LOG = LoggerFactory.getLogger(AvroMessageConsumer.class);

  @KafkaListener(topics = "#{'${kafka.topic.message}'}")
  public void consume(Message message) {
    LOG.info(String.format("#### -> Consumed message -> %s", message));
  }
}




