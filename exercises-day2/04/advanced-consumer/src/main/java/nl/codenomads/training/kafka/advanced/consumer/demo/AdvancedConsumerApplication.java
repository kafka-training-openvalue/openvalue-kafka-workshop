package nl.codenomads.training.kafka.advanced.consumer.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvancedConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvancedConsumerApplication.class, args);
	}

}
