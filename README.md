# OpenValue Kafka workshop

Exercises that allow you to see the theory learned in practice.
It is best to make the assignments in order, sometimes an assignment builds on one of the previous assignments.

In each exercise directory, you will find the exercise instructions
