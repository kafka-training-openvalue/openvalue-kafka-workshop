# Exercise 1: Offsets in Kafka Consumer

Exercise to experience the difference between manual and automatic offset commits.
Prereq: kafka cluster running with quickstart-events topic (see exercise 00)

1. A simple Consumer and Producer are already created, build the project and run both the producer and consumer and test if you can send and receive messages 
2. Check the javadoc of [KafkaConsumer](https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/consumer/KafkaConsumer.html) to know how to switch between manual and automatic offset committing.
3. Make sure the offset of each message is printed on screen, so you can keep track of the consumer behaviour.
4. Automatic offset commits:
    1. Ensure that auto commit is in use by the consumer.
    2. Simulate a fatal failure after reading 3 messages. For example, throw a runtime exception when reading the 4th message.
    3. Restart the consumer.
    4. What's the offset of the first message it receives?
5. Manual offset commits:
    1. Ensure that manual commit is in use by the consumer.
    2. Simulate a fatal failure after reading 3 messages. For example, throw a runtime exception when reading the 4th message.
    3. Restart the consumer.
    4. What's the offset of the first message it receives?

